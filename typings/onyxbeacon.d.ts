declare module "com" {

    module onyxbeacon {

    

    class BuildConfig {
        APPLICATION_ID: string;
        BUILD_TYPE: string;
        DEBUG: boolean;
        FLAVOR: string;
        VERSION_CODE: number;
        VERSION_NAME: string;
        extend()
    }

    class CouponDetailsActivity {}

    class OnyxBeaconApplication {
    }

    module OnyxBeaconApplication {
        function getOnyxBeaconManagerDebug(context) : OnyxBeaconManager;
        function getOnyxBeaconManager(context): OnyxBeaconManager;
        function extend();
        function startCouponDetailActivity();
        const PAYLOAD_TYPE: string;
        const TAG_TYPE: string;
        const BEACON_TYPE: string;
        const COUPON_TYPE: string;
        const PUSH_TYPE: string;
        const EXTRA_TAGS: string;
        const EXTRA_BEACONS: string;
        const EXTRA_COUPON: string;
        const EXTRA_BLUEMIX: string;

    }

    class OnyxBeaconManager {
        initSDK(AuthenticationMode: string);
        setCouponEnabled(boolean);
        setAPIContentEnabled(boolean);
    }



    class OnyxTagsListener {
        onTagsReceived(tagslist: Array<any>);
    }


    class BroadcastReceiver {
        protected mOnyxSDKListener: OnyxBeaconSDKListener;
    }

    class OnyxBeaconSDKListener { }

    module "listeners" {
        class OnyxPushListener { }
        class OnyxCouponsListener { }
        class OnyxTagsListener {
            onTagsReceived(tagslist: Array<any>);
        }
        class OnyxBeaconsListener {
            didRangeBeaconsInRegion(beacons: Array<any>);
        }

    }

    var service : {
        model: {
            Tag: string
        }
    }

    class IBeacon {

    }

    class Eddystone {

    }

    }
}