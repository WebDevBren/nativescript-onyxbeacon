/// <reference path="index.d.ts"/> 

import { Subject, Observable } from 'rxjs';

export class OnyxBeacon {

    static EVENTS : {
        BEACONS_FOUND: string
    } = {
        BEACONS_FOUND: 'BEACONS_FOUND'
    }

    protected _beaconEvent: Subject<any> = new Subject<any>();
    public beaconEvent: Observable<any> = this._beaconEvent.asObservable();

    beaconRecieved(localBeacons: any[]) {
        this._beaconEvent.next({ event: OnyxBeacon.EVENTS.BEACONS_FOUND, detail: localBeacons });
    }

}
