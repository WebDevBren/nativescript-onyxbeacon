import { ContentReceiver } from './components/ContentReceiver.android';
import { OnyxBeacon as OnyxBeaconCommon } from './onyxbeacon.common';
import { android } from 'application';

import { onyxbeacon } from 'com';

export class OnyxBeacon extends OnyxBeaconCommon {

    ContentReceiver: ContentReceiver = ContentReceiver.getInstance();
    Manager: onyxbeacon.OnyxBeaconManager = onyxbeacon.OnyxBeaconApplication.getOnyxBeaconManager(android.foregroundActivity)

    constructor() {
        super();

        this.Manager.initSDK('');
        this.Manager.setCouponEnabled(true);
        this.Manager.setAPIContentEnabled(true);

        this.ContentReceiver.onReceive;
    }

    initialize() {

    }

    onPause() {

    }

    onResume() {

    }
    
}