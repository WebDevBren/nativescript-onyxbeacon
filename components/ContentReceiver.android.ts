/// <reference path="../index.d.ts"/>

import { BroadcastReceiver, OnyxTagsListener, OnyxBeaconsListener, OnyxPushListener, OnyxBeaconSDKListener, OnyxBeaconApplication,} from 'com.onyxbeacon';

export class ContentReceiver extends BroadcastReceiver {

    private mOnyxBeaconListener : OnyxBeaconsListener;
    private mOnyxCouponsListener : OnyxCouponsListener;
    private mOnyxTagsListener : OnyxTagsListener;
    private mOnyxPushListener : OnyxPushListener;


    constructor(onyxBeaconSDKListener: OnyxBeaconSDKListener) {
        super();
        this.mOnyxSDKListener = onyxBeaconSDKListener;
    }

    public setOnyxBeaconsListener(onyxBeaconListener : OnyxBeaconsListener) : void {
        this.mOnyxBeaconListener = onyxBeaconListener;
    }

    public setOnyxCouponsListener(onyxCouponsListener: OnyxCouponsListener) : void {
        this.mOnyxCouponsListener = onyxCouponsListener;
    }

    public setOnyxTagsListener(onyxTagsListener : OnyxTagsListener) : void {
        this.mOnyxTagsListener = onyxTagsListener;
    }

    public setOnyxPushListener(onyxpushListener: OnyxPushListener) : void {
        this.mOnyxPushListener = onyxpushListener;
    }

    public onReceive(context, intent) : void {
        let payloadType: string = intent.getStringExtra(OnyxBeaconApplication.PAYLOAD_TYPE);

        switch (payloadType) {
            case OnyxBeaconApplication.TAG_TYPE: 
                let tagsList = intent.getParcelableArrayListExtra(OnyxBeaconApplication.EXTRA_TAGS);
                if(this.mOnyxTagsListener != null) {
                    this.mOnyxTagsListener.onTagsReceived(tagsList);
                } else {
                    //@TODO : In Background!
                }
            break;
            case OnyxBeaconApplication.BEACON_TYPE: 
                let beacons = intent.getParcelableArrayListExtra(OnyxBeaconApplication.EXTRA_BEACONS);
                if( this.mOnyxBeaconListener != null ) {
                    this.mOnyxBeaconListener.didRangeBeaconsInRegion(beacons);
                } else {
                    /**
                     * TODO: Background Mode!
                     */
                }
            break;
            case OnyxBeaconApplication.COUPON_TYPE: 
                let coupon = intent.getParcelableExtra(OnyxBeaconApplication.EXTRA_COUPON);
                if( this.mOnyxCouponsListener != null ) {
                    console.error('NOT IMPLEMENTED');
                    // this.mOnyxCouponsListener.onCouponReceived(coupon, beacon);
                }
            break;
            case OnyxBeaconApplication.PUSH_TYPE: 

            break;
        }
    }

}